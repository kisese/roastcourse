<?php
/**
 * Created by PhpStorm.
 * User: briankisese
 * Date: 11/10/2018
 * Time: 10:53
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AppController extends Controller
{
    public function getApp()
    {
        return view('app');
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
